/**
 * Convert timecodes:string to seconds:number
 */
export function tcToSec(tc) {
	const sec = tc
		.split(':')
		// Convert individual parts to floats
		.map(parseFloat)
		// Aggregate minutes and hours to seconds
		.reverse()
		.map((n, i) => Math.pow(60, i) * n)
		.reduce((a, b) => a + b);

	return Math.round(sec * 1000) / 1000;
}

/**
 * Convert seconds:number to timecodes:string
 */
export function secToTc(s) {
	const pad = (n, z = '00') => (z + n).slice(-z.length);
	const hours = Math.floor(s / 60 / 60);
	const mins = Math.floor(s / 60 - hours * 60);
	const tail = s - mins * 60 - hours * 60 * 60;
	const secs = Math.floor(tail);
	const millis = Math.round(1000 * (tail - secs));

	return `${pad(hours)}:${pad(mins)}:${pad(secs)}.${pad(millis, '000')}`;
}